package com.paraProbarJar.paraProbarJar;
import java.lang.System;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParaProbarJarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParaProbarJarApplication.class, args);
		System.out.println("Esta app <<paraProbarJar>> está  corriendo!!!!!.");
	}

}
