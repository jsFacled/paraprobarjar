package com.paraProbarJar.paraProbarJar;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GlobalController {

    @RequestMapping("/hello")
    public String helloWorld() {
        return "Hello, World! -- Aquí la App para probar Jar está corriendo exitosamente!!!";
    }
}